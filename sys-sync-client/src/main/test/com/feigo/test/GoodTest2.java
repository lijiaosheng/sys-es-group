package com.feigo.test;

import com.feigu.entity.Category;
import com.feigu.entity.Goods;
import com.feigu.entity.SmallImage;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;


@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration({"classpath*:spring-context.xml"})
public class GoodTest2 {


    @Before
    public void before() {
    }

    @Test
    public void test() {
        Category category1 = new Category();
        category1.setId(1L);
        category1.setCatName("护肤");
        Category category2 = new Category();
        category2.setId(2L);
        category2.setCatName("保湿");
        Goods goods = new Goods();
        goods.setCategories(Arrays.asList(category1, category2));
        goods.setNumIid(432132L);
        goods.setReservePrice("23d");
        goods.setPictUrl("hfudhfuiewhfuewufhuew");
        goods.setItemUrl("fhewiufhuiewfuweufuweu");
        goods.setNick("hello");
        goods.setId("321312");
        goods.setProvcity("上海");
        goods.setSellerId(2312L);
        goods.setShopTitle("控油祛痘");
        SmallImage image1=new SmallImage();
        image1.setId("fehfeuwfyuwue");
        image1.setSmallImageUrl("https://img.alicdn.com/tps/i2/TB1XQjmJpXXXXcdXXXX9MF13pXX-540-660.jpg_250x250q90.jpg");
        SmallImage image2=new SmallImage();
        image2.setId("dhsiahdashudiu");
        image2.setSmallImageUrl("https://img.alicdn.com/tps/i4/TB16VxKcvBNTKJjy0FdSuwPpVXa.jpg_1080x1800Q60s50.jpg");
        goods.setSmallImages(Arrays.asList(image1,image2));
        goods.setTitle("欧莱雅");
        goods.setTkRate("dhashdiuasudu");
        goods.setUserType(321321L);
        goods.setVolume(150L);
        goods.setZkFinalPrice("dhausdu231");
    }


}
