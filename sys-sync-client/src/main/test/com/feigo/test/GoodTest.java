package com.feigo.test;

import com.feigu.entity.Category;
import com.feigu.entity.Goods;
import com.feigu.entity.SmallImage;
import com.feigu.repository.good.GoodRepository;
import com.feigu.utils.JSONUtils;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.elasticsearch.core.query.IndexQuery;
import org.springframework.data.elasticsearch.core.query.IndexQueryBuilder;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.sql.Date;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;


@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration({"classpath*:spring-context.xml"})
public class GoodTest {

    @Autowired
   GoodRepository repository;

    @Before
    public void before() {
        repository.createIndex();
    }
    @Test
    public void test() {
        Category category1=new Category();
        category1.setId(1L);
        category1.setCatName("护肤");
        category1.setCatId(23123l);
        category1.setPid(3123L);
        category1.setCreateAt(new Date(312312421L));
        category1.setCreateBy("dsad");
        category1.setModifyAt(new Date(321312L));
        category1.setModifyBy("dasdsa");
        Category category2=new Category();
        category2.setId(2L);
        category2.setCatId(23123l);
        category2.setPid(3123L);
        category2.setCreateAt(new Date(312312421L));
        category2.setCreateBy("dsad");
        category2.setModifyAt(new Date(321312L));
        category2.setModifyBy("dasdsa");
        category2.setCatName("保湿");
        Goods goods=new Goods();
        goods.setCreateAt(new Date(32173218l));
        goods.setCreateBy("dsad");
        goods.setModifyAt(new Date(321312L));
        goods.setModifyBy("dasdsa");
        goods.setCategories(Arrays.asList(category1, category2));
        goods.setNumIid(432132L);
        goods.setReservePrice("23d");
        goods.setPictUrl("hfudhfuiewhfuewufhuew");
        goods.setItemUrl("fhewiufhuiewfuweufuweu");
        goods.setNick("hello");
        goods.setId("321312");
        goods.setProvcity("上海");
        goods.setSellerId(2312L);
        goods.setShopTitle("控油dewefw祛痘");
        SmallImage image1=new SmallImage();
        image1.setId("fehfeuwfyuwue");
        image1.setSmallImageUrl("https://img.alicdn.com/tps/i2/TB1XQjmJpXXXXcdXXXX9MF13pXX-540-660.jpg_250x250q90.jpg");
        SmallImage image2=new SmallImage();
        image2.setId("dhsiahdashudiu");
        image2.setSmallImageUrl("https://img.alicdn.com/tps/i4/TB16VxKcvBNTKJjy0FdSuwPpVXa.jpg_1080x1800Q60s50.jpg");
        goods.setSmallImages(Arrays.asList(image1,image2));
        goods.setTitle("欧莱雅dwheiw");
        goods.setTkRate("dhashdiuasudu");
        goods.setUserType(321321L);
        goods.setVolume(150L);
        goods.setZkFinalPrice("dhausdu231");
        IndexQuery indexQuery = new IndexQueryBuilder().withId(goods.getId()).withObject(goods).build();
        repository.index(indexQuery);
    }


    @Test
    public void deleteGood(){
        repository.delete("321312");
    }

    @Test
    public void updateGood(){
        repository.update(new Goods());
    }

    @Test
    public void searchGoodsByCatName(){
        List<Goods> list=repository.searchGoodsByCatName("运动");
        System.out.println(JSONUtils.toJson(list));
    }


}
