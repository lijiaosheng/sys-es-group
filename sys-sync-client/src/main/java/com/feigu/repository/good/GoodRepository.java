package com.feigu.repository.good;


import com.feigu.entity.Goods;
import org.springframework.data.elasticsearch.core.query.IndexQuery;

import java.util.List;

/**
 * Created by lijiaosheng on 2017/10/12.
 */
public interface GoodRepository  {
    public  void createIndex();
    public void delete(String id);
    public  void index(IndexQuery query);
    public void update(Goods goods);
    public List<Goods> searchGoodsByCatName(String catName);
}
