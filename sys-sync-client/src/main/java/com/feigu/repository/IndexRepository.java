package com.feigu.repository;

import org.springframework.data.elasticsearch.core.query.CriteriaQuery;

import java.util.Map;

/**
 * Created by lijiaosheng on 2017/10/12.
 */
public interface IndexRepository {

    /**
     * 创建索引
     * @param clazz 实体类
     */
    Boolean createIndex(Class clazz);

    /**
     * 删除索引
     * @param clazz 实体类
     */
    Boolean deleteIndex(Class clazz);


    /**
     * 刷新索引
     * @return
     */
    void flushIndex(Class clazz);

    /**
     * 判断索引是否存在
     * @return
     */
    Boolean indicesExists(String indexName);

    /**
     * 创建文档
     * **/

    Boolean createDocument(Class clazz);
    /**
     * 更新文档
     * **/
    Boolean updateDocument(Class clazz);

    /**
     * 获取文档
     * */
    <T> Map getDocument(Class clazz);

    /**
     * 根据传入的条件获取文档总数
     * */
    void countDocumentSize(CriteriaQuery query);
  }
