package com.feigu.repository.impl;

import com.feigu.repository.IndexRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.elasticsearch.core.ElasticsearchTemplate;
import org.springframework.data.elasticsearch.core.query.CriteriaQuery;
import org.springframework.stereotype.Component;

import java.util.Map;

/**
 * 自定义Repository实现类
 *
 * 接口的实现类名字后缀必须为impl才能在扫描包时被找到（可参考spring data elasticsearch自定义repository章节）
 *
 */
@Component("indexRepositoryImpl")
public class IndexRepositoryImpl implements IndexRepository {

    @Autowired
    private ElasticsearchTemplate elasticsearchTemplate;


    public Boolean createIndex(Class clazz) {
      return elasticsearchTemplate.createIndex(clazz);
    }


    public Boolean deleteIndex(Class clazz) {
      return   elasticsearchTemplate.deleteIndex(clazz);
    }


    public void flushIndex(Class clazz) {
        elasticsearchTemplate.refresh(clazz);
    }

    public Boolean indicesExists(String indexName) {
       return elasticsearchTemplate.indexExists(indexName);
    }

    public Boolean createDocument(Class clazz) {
        return elasticsearchTemplate.putMapping(clazz);
    }

    public Boolean updateDocument(Class clazz) {
//        UpdateQuery query=new UpdateQuery();
//        elasticsearchTemplate.update();
        return null;
    }

    public <T> Map getDocument(Class clazz) {
    return elasticsearchTemplate.getMapping(clazz);
    }

    public void countDocumentSize(CriteriaQuery query) {
        elasticsearchTemplate.count(query);
    }

}