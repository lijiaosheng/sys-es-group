package com.feigu.entity;

import org.springframework.data.annotation.Id;
import org.springframework.data.elasticsearch.annotations.DateFormat;
import org.springframework.data.elasticsearch.annotations.Document;
import org.springframework.data.elasticsearch.annotations.Field;
import org.springframework.data.elasticsearch.annotations.FieldType;

import java.sql.Date;
import java.sql.Timestamp;
import java.util.List;

/**
 * @描述:
 * @作者 骆文龙
 * @创建时间 2017-10-02.
 * @版本: v1.0
 */
@Document(indexName = "goods", type = "goods-type", shards = 2, replicas = 0)
public class Goods {
    @Id
    private String id;
    @Field(type = FieldType.text)
    private String itemUrl;//商品地址
    @Field(type = FieldType.text)
    private String nick;//卖家昵称
    @Field(type = FieldType.Long)
    private Long numIid;//商品ID
    @Field(type = FieldType.text)
    private String pictUrl;//商品主图
    @Field(type = FieldType.text)
    private String provcity;//宝贝所在地
    @Field(type = FieldType.text)
    private String reservePrice;//商品一口价格
    @Field(type = FieldType.Long)
    private Long sellerId;//卖家id
    @Field(type = FieldType.text)
    private String shopTitle;
    @Field(type = FieldType.Nested)
    private List<SmallImage> smallImages;//商品小图列表
    @Field(type = FieldType.text)
    private String title;//商品标题
    @Field(type = FieldType.text)
    private String tkRate;
    @Field(type = FieldType.Long)
    private Long userType;//卖家类型，0表示集市，1表示商城
    @Field(type = FieldType.Long)
    private Long volume;//30天销量
    @Field(type = FieldType.text)
    private String zkFinalPrice;//商品折扣价格
    @Field(type = FieldType.Nested)
    private List<Category> categories;
    @Field(type = FieldType.text)
    private String createBy;
    @Field(type = FieldType.Date,format = DateFormat.custom, pattern = "yyyy-MM-dd HH:mm:ss")
    private Date createAt;
    @Field(type = FieldType.Date,format = DateFormat.custom, pattern = "yyyy-MM-dd HH:mm:ss")
    private Date modifyAt;
    @Field(type = FieldType.text)
    private String modifyBy;

    public String getCreateBy() {
        return createBy;
    }

    public void setCreateBy(String createBy) {
        this.createBy = createBy;
    }

    public Date getCreateAt() {
        return createAt;
    }

    public void setCreateAt(Date createAt) {
        this.createAt = createAt;
    }

    public Date getModifyAt() {
        return modifyAt;
    }

    public void setModifyAt(Date modifyAt) {
        this.modifyAt = modifyAt;
    }

    public String getModifyBy() {
        return modifyBy;
    }

    public void setModifyBy(String modifyBy) {
        this.modifyBy = modifyBy;
    }

    public List<Category> getCategories() {
        return categories;
    }

    public void setCategories(List<Category> categories) {
        this.categories = categories;
    }

    public String getItemUrl() {
        return itemUrl;
    }

    public void setItemUrl(String itemUrl) {
        this.itemUrl = itemUrl;
    }

    public String getNick() {
        return nick;
    }

    public void setNick(String nick) {
        this.nick = nick;
    }

    public Long getNumIid() {
        return numIid;
    }

    public void setNumIid(Long numIid) {
        this.numIid = numIid;
    }

    public String getPictUrl() {
        return pictUrl;
    }

    public void setPictUrl(String pictUrl) {
        this.pictUrl = pictUrl;
    }

    public String getProvcity() {
        return provcity;
    }

    public void setProvcity(String provcity) {
        this.provcity = provcity;
    }

    public String getReservePrice() {
        return reservePrice;
    }

    public void setReservePrice(String reservePrice) {
        this.reservePrice = reservePrice;
    }

    public Long getSellerId() {
        return sellerId;
    }

    public void setSellerId(Long sellerId) {
        this.sellerId = sellerId;
    }

    public String getShopTitle() {
        return shopTitle;
    }

    public void setShopTitle(String shopTitle) {
        this.shopTitle = shopTitle;
    }

    public List<SmallImage> getSmallImages() {
        return smallImages;
    }

    public void setSmallImages(List<SmallImage> smallImages) {
        this.smallImages = smallImages;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getTkRate() {
        return tkRate;
    }

    public void setTkRate(String tkRate) {
        this.tkRate = tkRate;
    }

    public Long getUserType() {
        return userType;
    }

    public void setUserType(Long userType) {
        this.userType = userType;
    }

    public Long getVolume() {
        return volume;
    }

    public void setVolume(Long volume) {
        this.volume = volume;
    }

    public String getZkFinalPrice() {
        return zkFinalPrice;
    }

    public void setZkFinalPrice(String zkFinalPrice) {
        this.zkFinalPrice = zkFinalPrice;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }


    @Override
    public String toString() {
        return "Goods{" +
                "id='" + id + '\'' +
                ", itemUrl='" + itemUrl + '\'' +
                ", nick='" + nick + '\'' +
                ", numIid=" + numIid +
                ", pictUrl='" + pictUrl + '\'' +
                ", provcity='" + provcity + '\'' +
                ", reservePrice='" + reservePrice + '\'' +
                ", sellerId=" + sellerId +
                ", shopTitle='" + shopTitle + '\'' +
                ", smallImages=" + smallImages +
                ", title='" + title + '\'' +
                ", tkRate='" + tkRate + '\'' +
                ", userType=" + userType +
                ", volume=" + volume +
                ", zkFinalPrice='" + zkFinalPrice + '\'' +
                ", categories=" + categories +
                '}';
    }
}
