package us.codecraft.webmagic.config;

import com.taobao.api.request.*;

/**
 * @描述： 免费的淘宝API
 * @作者： Haiwei.Zhang
 * @创建时间： 2017-09-27
 * @版本： 1.0
 */
public enum FreeTaobaoAPIEnum {

    ITEM_GET("taobao.tbk.item.get", "淘宝客商品查询", TbkItemGetRequest.class),
    ITEM_RECOMMEND_GET("taobao.tbk.item.recommend.get", "淘宝客商品关联推荐查询", TbkItemRecommendGetRequest.class),
    ITEM_INFO_GET("taobao.tbk.item.info.get", "淘宝客商品详情（简版）", TbkItemInfoGetRequest.class),
    SHOP_GET("taobao.tbk.shop.get", "淘宝客店铺查询", TbkShopGetRequest.class),
    SHOP_RECOMMEND_GET("taobao.tbk.shop.recommend.get", "淘宝客店铺关联推荐查询", TbkShopRecommendGetRequest.class),
    EVENT_GET("taobao.tbk.uatm.event.get", "枚举正在进行中的定向招商的活动列表", TbkUatmEventGetRequest.class),
    EVENT_ITEM_GET("taobao.tbk.uatm.event.item.get", "获取淘宝联盟定向招商的宝贝信息", TbkUatmEventItemGetRequest.class),
    FAVORITES_ITEM_GET("taobao.tbk.uatm.favorites.item.get", "获取淘宝联盟选品库的宝贝信息", TbkUatmFavoritesItemGetRequest.class),
    FAVORITES_GET("taobao.tbk.uatm.favorites.get", "获取淘宝联盟选品库列表", TbkUatmFavoritesGetRequest.class),
    TQG_GET("taobao.tbk.ju.tqg.get", "淘抢购API", TbkJuTqgGetRequest.class),
    SPREAD_GET("taobao.tbk.spread.get", "物料传播方式获取", TbkSpreadGetRequest.class),
    ITEMS_SEARCH("taobao.ju.items.search", "聚划算商品搜索接口", JuItemsSearchRequest.class),
    ITEM_COUPON_GET("taobao.tbk.dg.item.coupon.get", "好券清单API【导购】", TbkDgItemCouponGetRequest.class),
    COUPON_GET("taobao.tbk.coupon.get", "阿里妈妈推广券信息查询", TbkCouponGetRequest.class),
    TPWD_CREATE("taobao.tbk.tpwd.create", "淘宝客淘口令", TbkTpwdCreateRequest.class);

    private String code;
    private String name;
    private Class clazz;

    FreeTaobaoAPIEnum(String code, String name, Class clazz) {
        this.code = code;
        this.name = name;
        this.clazz = clazz;
    }

    public String getCode() {
        return code;
    }

    public String getName() {
        return name;
    }

    public Class getClazz() {
        return clazz;
    }
}
