package us.codecraft.webmagic.config;

/**
 * @描述： Config 的实体类
 * @作者： Haiwei.Zhang
 * @创建时间： 2017-09-27
 * @版本： 1.0
 */
public class Config {
    //API服务地址沙箱环境
    public static final String TAOBAO_URL_TEST = "https://gw.api.tbsandbox.com/router/rest";
    //消息服务地址沙箱环境
    public static final String TAO_BAO_WS_TEST = "ws://mc.api.tbsandbox.com/";
    //API服务地址正式环境
    public static final String TAOBAO_URL = "https://eco.taobao.com/router/rest";
    //消息服务地址正式环境
    public static final String TAOBAO_WS = "ws://mc.api.taobao.com/";
    //淘宝客app_key
    public static final String APP_KEY = "24632852";
    //淘宝客app_secret
    public static final String APP_SECRET = "79d8e3891149e23ebe412fadf6859194";
}
